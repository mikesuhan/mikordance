from itertools import chain
from text_processing.tokenizers import word_tokenize
from text_processing.matcher import is_match

class Text:
    """Processes a single text."""
    def __init__(self, parent, filepath, encoding='UTF-8', item_delimiter=' '):
        self.parent = parent
        self.name = self.parent.name
        self.filepath = filepath
        self.encoding = encoding
        self.freq_dist = None
        self.token_count = 0

        with open(filepath, 'r', encoding=encoding, errors='ignore') as f:
            self.text = f.read()
        # this is not an accurate count of tokens.
        # It is used to get an estimate of the average word count in the corpus to guess what to norm frequencies to
        self.rough_token_count = self.text.count(item_delimiter)

    def tokens(self, tokenizer=word_tokenize, **kwargs):
        tokens = tokenizer(self.text, **kwargs)
        self.token_count = len(tokens)
        return tokens

    def ngrams(self, n, **kwargs):
        if n == 1:
            return tuple((token,) for token in self.tokens())
        elif type(n) is int:
            tokens = self.tokens(**kwargs)
            return tuple(tuple(tokens[i:i + n]) for i, t in enumerate(tokens) if len(tokens) >= i + n)
        elif type(n) in [tuple, list]:
            return tuple(chain(*(self.ngrams(k, **kwargs) for k in n)))

    def freqDist(self, ngram_len, *query_tokens, case_sensitive=False, sort=False, norm_to=None, tokenizer=word_tokenize):
        """Makes a dictionary where keys are tuples of tokens or ngrams and values are their frequency in the text.
        
        Arguments:
            ngram_len: 2-item tuple of range of ngram lengths
            *query_tokens: tokens that are searched for. Limits frequencies to only these items.
        """
        # Makes lowercase or doesn't
        if not case_sensitive:
            funcs = [str.lower]
            if query_tokens:
                query_tokens = tuple(tuple(t.lower() for t in tok) for tok in query_tokens)
        else:
            funcs = None

        if query_tokens:
            ngram_len = [len(item) for item in query_tokens]
        elif ngram_len[0] == ngram_len[1]:
            ngram_len = ngram_len[0]
        else:
            ngram_len = list(range(ngram_len[0], ngram_len[1] + 1))

        ngrms = self.ngrams(ngram_len, tokenizer=tokenizer, funcs=funcs)
        results = {}

        # decides if everything is counted or if just query_tokens items is counted
        if query_tokens:
            search_types = query_tokens
        else:
            search_types = ngrms

        # counts items and makes frequency dsitribution
        for search_type in set(search_types):
            # c = ngrms.count(ngrm)  --- maybe add this back in later for if there is no | or * in the search
            # c = len([ng for ng in ngrms if is_match(ng, search_type)])
            c = 0
            for ng in ngrms:
                if is_match(ng, search_type):
                    results[ng] = results.get(ng, 0) + 1

                    # makes freq dist with same keys for entire corpus
                    if self.parent.counting_all:
                        self.parent.corpus_freq_dist[ng] += 1

                    c += 1

        # normalizes frequencies
        if norm_to:
            results = {key: results[key] / self.token_count * norm_to for key in results}

        if sort:
            return sorted(((k, results[k]) for k in results), key=lambda x:x[1], reverse=True)

        return results
