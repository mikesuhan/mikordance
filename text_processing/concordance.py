from random import shuffle
from PyQt5.QtCore import QCoreApplication
from text_processing.tokenizers import word_tokenize
from text_processing.matcher import is_match

def concordance(query, *texts, left_len=10, right_len=10, case_sensitive=False, random_order=False, limit=None,
                tokenizer=str.split):
    """Yields tuple of strings of left, center, and right word(s) in concordance line.

    >>> import os
    >>> from text_processing.corpus import Corpus
    >>> from text_processing.concordance import concordance
    >>> s = [os.path.join('/home/mike/PycharmProjects/random_wiki/english_wikipedia', f) for f in os.listdir('/home/mike/PycharmProjects/random_wiki/english_wikipedia')]
    >>> corpus = Corpus([os.path.join('sample', f) for f in s], 'example')
    >>> c = concordance('the', *[t for t in corpus.texts])
    >>> next(c)
    """
    query = word_tokenize(query.strip())

    if random_order:
        texts = list(texts)
        shuffle(texts)

    lines_n = 0
    texts_len = len(texts)

    for k, text in enumerate(texts):
        text.parent.parent.message('({}/{}) {}% of texts processed.'.format(k + 1, texts_len, round((k + 1) / texts_len * 100)))
        tokens = text.tokens(tokenizer=tokenizer)

        if lines_n == limit:
            break

        for i, token in enumerate(tokens):
            QCoreApplication.processEvents()
            if lines_n == limit:
                break

            if len(tokens) >= i + len(query) and is_match(tokens[i:i + len(query)], query, case_sensitive=case_sensitive):
                if left_len > i:
                    left_i = 0
                else:
                    left_i = i - left_len
                right_i = i + len(query) + right_len

                left_conc = ' {} '.format(' '.join(tokens[left_i:i]))
                center_conc = ' {} '.format(' '.join(tokens[i:i + len(query)]))
                right_conc = ' {} '.format(' '.join(tokens[i + len(query):right_i]))

                lines_n += 1

                yield text, left_conc, center_conc, right_conc, i, i + len(query)
