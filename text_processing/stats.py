from statistics import median
from math import sqrt
from collections import Counter, OrderedDict

class Descriptives:
    """Calculates descriptive statistics. Avoids repeated calls to the same function."""
    def __init__(self, array, *procedures, round_to=2):
        self.results = {}

        n = len(array)

        if 'Dispersion':
            self.results['Dispersion'] = len([x for x in array if x])

        if 'Mean' in procedures \
            or 'Sample SD' in procedures or 'Pop. SD' in procedures \
            or 'Sample Variance' in procedures or 'Pop. Variance' in procedures:
            _mean = sum(array) / n
            if 'Mean' in procedures:
                self.results['Mean'] = _mean

        if 'Median' in procedures:
            self.results['Median'] = median(array)

        if 'Mode' in procedures:
            self.results['Mode'] = Counter(array).most_common(1)[0][0]

        if 'Range' in procedures or 'Minimum' in procedures:
            _minimum = min(array)
            if 'Minimum' in procedures:
                self.results['Minimum'] = _minimum

        if 'Range' in procedures or 'Maximum' in procedures:
            _maximum = max(array)
            if 'Maximum' in procedures:
                self.results['Maximum'] = _maximum

        if 'Range' in procedures:
            self.results['Range'] = _maximum - _minimum

        if 'Sample Variance' in procedures or 'Sample SD' in procedures:
            _variance = sum((x-_mean)**2 for x in array) / (n - 1)
            if 'Sample Variance' in procedures:
                self.results['Sample Variance'] = _variance

        if 'Pop. Variance' in procedures or 'Pop. SD' in procedures:
            _pvariance = sum((x - _mean) ** 2 for x in array) / (n)
            if 'Pop. Variance' in procedures:
                self.results['Pop. Variance'] = _pvariance

        if 'Sample SD' in procedures:
            self.results['Sample SD'] = sqrt(_variance)


        if 'Pop. SD' in procedures:
            self.results['Pop. SD'] = sqrt(_pvariance)

