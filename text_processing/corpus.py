from collections import Counter, defaultdict
from text_processing.text import Text
from text_processing.stats import Descriptives
from statistics import mean, median, mode, stdev, pstdev, variance, pvariance
from PyQt5.QtCore import Qt, QCoreApplication
from itertools import chain

class Corpus:
    """
    Consists of multiple texts.
    
    >>> import os
    >>> from text_processing.corpus import Corpus
    >>> fps = [os.path.join('/home/mike/PycharmProjects/random_wiki/conservapedia', f) for f in os.listdir('/home/mike/PycharmProjects/random_wiki/conservapedia')]     
    >>> corpus = Corpus(fps, 'test_corpus')

    """
    def __init__(self, parent, file_paths, name, tag_delimiter='', item_delimiter=' ', encoding=None):
        self.parent = parent
        self.name = name
        self.tag_delimiter = tag_delimiter
        self.item_delimiter = item_delimiter
        self.encoding = encoding
        self.file_paths = file_paths
        self.texts = []
        self.corpus_freq_dist = defaultdict(int)
        self.norm_to = 1000
        self.loadTexts(file_paths)
        self.counting_all = False
        self.token_count = 0

    def loadTexts(self, paths):
        self.texts = []
        self.rough_token_counts = []
        self.file_paths = paths
        rough_token_counts = []
        for i, fp in enumerate(self.file_paths):
            self.texts.append(Text(self, fp))
            rough_token_counts.append(self.texts[i].rough_token_count)

        mean_count = mean(rough_token_counts)
        digits = len(str(int(mean_count)))
        self.norm_to = 10 ** digits

    def freqDist(self, **kwargs):
        """Returns dictionary of frequency distribution for every token in the corpus corpus."""
        counter = Counter()
        for text in self.texts:
            freq_dist = text.freqDist(**kwargs)
            counter.update(freq_dist)
        return counter

    def threadedFreqLists(self, ngram_len, *tokens, auto_norm=True, norm_rate=None, **kwargs):
        pass

    def freqLists(self, ngram_len, *tokens, auto_norm=True, norm_rate=None, **kwargs):
        """Returns list of frequencies for every token in the corpus."""
        self.counting_all = True
        if auto_norm:
            norm_rate = self.norm_to

        freq_lists = defaultdict(list)
        texts_len = len(self.texts)

        # not all tokens will be present in self.corpus_freq_dist for entire loop
        for i, text in enumerate(self.texts):
            # Stops GUI from locking up
            QCoreApplication.processEvents()
            freq_dist = text.freqDist(ngram_len, *tokens, norm_to=norm_rate, **kwargs)
            for key in self.corpus_freq_dist:
                freq_lists[key].append(freq_dist.get(key, 0))
            self.parent.message('({}/{}) {}% of texts loaded.'.format(i, texts_len, round(i / texts_len * 100)))

        # adds in 0 values for tokens that were not yet in self.corpus_freq_dist when token was iterated over above
        for key in freq_lists:
            QCoreApplication.processEvents()
            # number of items in the freq_list -- should be equal to number of texts in corpus.
            fl_len = len(freq_lists[key])
            # number of texts in the corpus -- difference between this and length of the freq_list is filled with 0s
            # for the texts that did not have the matching token with an index in corpus.texts earlier than a text
            # in which the token occurs
            t_len = len(self.texts)
            if fl_len != t_len:
                freq_lists[key] += [0] * (t_len - fl_len)

        self.counting_all = False

        return freq_lists



    def stats(self, ngram_len, *tokens, procedures_list=None, format_results=True, round_to=2, **kwargs):
        """Returns dictionary of dictionaries of every statistic in procedures done on every token in freq_lists."""

        easy_procedures = [pr for pr in procedures_list if pr in ['Frequency', 'Rate', 'Dispersion']]
        # Uses different algorithm for statistics that don't take as much resources to process
        if len(procedures_list) == len(easy_procedures):
            procedures = {
                'Frequency': lambda f, c, nt, r: f,
                'Rate': lambda f, c, nt, r: r(f / c * nt, 2),
                'Dispersion': lambda f, c, nt, r: f,
            }

            token_count = 0
            frequencies = defaultdict(int)
            dispersion = defaultdict(int)

            for i, text in enumerate(self.texts):
                if 'Dispersion' in procedures:
                    types = []
                QCoreApplication.processEvents()
                self.parent.message('{}%'.format(int((i+1) / len(self.texts) * 100)))

                for token in text.tokens(funcs=[str.lower]):
                    frequencies[token] += 1
                    token_count += 1
                    if 'Dispersion' in procedures and token not in types:
                        types.append(token)
                        dispersion[token] += 1

            for key in frequencies:
                    row = [key]
                    if 'Rate' in procedures_list:
                        row.append('%2f' % (frequencies[key] / token_count * 100))
                    if 'Frequency' in procedures_list:
                        row.append(frequencies[key])
                    if 'Dispersion' in procedures_list:
                        row.append(dispersion[key])

                    yield row


        elif procedures_list:
            # resets class variable
            self.corpus_freq_dist = defaultdict(int)

            output_rate, output_frequency = False, False
            if 'Rate' in procedures_list:
                output_rate = True
            if 'Frequency' in procedures_list:
                output_frequency = True

            procedures_list = [proc for proc in procedures_list if proc not in ['Rate', 'Frequency']]
            # procedures = {k:procedures[k] for k in procedures if k in procedures_list}




            # list of rates or frequencies for each type in the corpus
            freq_lists = self.freqLists(ngram_len, *tokens, **kwargs)
            # print(freq_lists)

            # number of tokens in the corpus
            if output_rate:
                corpus_token_count = sum(t.token_count for t in self.texts)

            # number of types in the corpus
            cfd_len = len(self.corpus_freq_dist.keys())

            # for i, token in enumerate(self.corpus_freq_dist.keys()):
            for i, token in enumerate(freq_lists):
                # GUI interactions
                QCoreApplication.processEvents()
                self.parent.message('{}% of tokens analyzed.'.format(round(i/cfd_len * 100)))

                descriptives = Descriptives(freq_lists[token], *procedures_list)
                row = [' '.join(token)] + [round(descriptives.results[k], 2) for k in procedures_list]

                if output_frequency or output_rate:
                    frequency = self.corpus_freq_dist.get(token, 0)
                    if output_frequency:
                        row.insert(1, frequency)
                    if output_rate:
                        row.insert(1, round(frequency / corpus_token_count * self.norm_to, 2))
                yield row




"""
import os
from text_processing.corpus import Corpus
fps = [os.path.join('/home/mike/PycharmProjects/random_wiki/conservapedia', f) for f in os.listdir('/home/mike/PycharmProjects/random_wiki/conservapedia')]     
corpus = Corpus(None, fps[:10], 'test_corpus')
s = corpus.stats(1, ('the', ), procedures_list=['Mean'])
"""