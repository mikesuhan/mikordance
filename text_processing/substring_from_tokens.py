"""
Finds position of ngram inside of a substring.
"""



from text_processing.tokenizers import word_tokenize

with open('/home/mike/PycharmProjects/mikordance/td/td1/1872_6_157.txt') as f:
    ex_text = f.read()

ex_tokens = word_tokenize(ex_text)

# 235 238 in addition ,

from re import finditer
from text_processing.lexicon import CONTRACTIONS, ENCLOSERS, PUNCTUATION, SYMBOLS


def get_substring_inds(text, tokens, token_i, token_end_i):

    """
    Returns indices of substring of a text corresponding to the indices of the phrase in the text.
    
    Used to highlight a word in a text that was clicked on in a QTableWidgetItem 
    in gui.concordance_window.ConcordanceWindow
    """
    phrase = tokens[token_i:token_end_i]

    # all indices in tokens matching the phrase
    matching_inds = [i for i, tok in enumerate(tokens) if tokens[i:i + (token_end_i - token_i)] == phrase]
    # the position of the particular phrase being searced for (coming from the concordance QTableWidget)
    token_position = matching_inds.index(token_i)

    # substring matches of the phrase in text
    # regexp = r'\b' + r'\s+'.join(phrase) + r'\b'  <<< list comprehension won't work because contractions are split
    print(phrase)
    if phrase[0].lower() not in CONTRACTIONS and phrase[0] not in ENCLOSERS + PUNCTUATION + SYMBOLS:
        regexp = r'\b'
    else:
        regexp = ''

    for i, token in enumerate(phrase):

        if i > 0 and token.lower() not in CONTRACTIONS and token not in ENCLOSERS + PUNCTUATION:
            regexp += r'\s+'

        # escapes special character that might be in string for regexp match
        # important to put \ at beginning of string or anything before \ will become \\ when escaped
        for char in '\.^$*+?()[{|':
            token = token.replace(char, '\\' + char)


        regexp += token

    # will not match with \b at the end if it is after punctuation, but punctuation will still be attached to
    # titles and abbreviations eg. mr. dr. st.
    if phrase[-1][-1] not in ENCLOSERS + PUNCTUATION + SYMBOLS:
        regexp += r'\b'

    print('regexp', regexp)

    substrings = list(finditer(regexp, text))
    print(substrings)
    return substrings[token_position].span()

def emphasize_substring(text, span):
    text = '{text_left}<span style="font-weight:1000; color:#FF0000;">{substring}</span>{text_right}'.format(
        text_left=text[:span[0]],
        substring=text[span[0]:span[1]],
        text_right=text[span[1]:]
    )

    return text.replace('\n', '<br>')
