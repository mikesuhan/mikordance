from text_processing.lexicon import ENCLOSERS, PUNCTUATION, CONTRACTIONS, TITLES


def word_tokenize(text, funcs=None):
    if funcs:
        for func in funcs:
            text = func(text)
    tokens = text.split()
    for i, token in enumerate(tokens):
        # punctuation at beginning of a word
        while len(tokens[i]) > 1 and tokens[i][0] in ENCLOSERS:
            if token.lower().startswith(CONTRACTIONS):
                break
            # inserts removed punctuation before the token
            tokens.insert(i, tokens[i][0])
            i += 1
            tokens[i] = tokens[i][1:]

        # punctuation at end of a word
        while len(tokens[i]) > 1 and tokens[i][-1] in ENCLOSERS + PUNCTUATION:
            # avoids splitting . from titles, e.g.: dr., mr., ave.
            if tokens[i].lower() in TITLES:
                break
            # avoids splitting punctuation followed by | operator used in search
            if tokens[i][-2] == '|':
                break
            # avoids splitting . from acronyms and initialisms with . in the middle, e.g.: e.g., U.S.S.R., B.C.
            elif tokens[i][-1] == '.' and len(tokens[i]) % 2 == 0 and len(tokens[i]) >= 4:
                if set(tokens[i][1::2]) == {'.'} and '.' not in set(tokens[i][0::2]):
                    break
            # Inserts removed punctuation after the token
            tokens.insert(i + 1, tokens[i][-1])
            tokens[i] = tokens[i][:-1]

        # contractions -- must come after punctuation is split from end of word
        for cont in CONTRACTIONS:
            if token.lower().endswith(cont) and token.lower() != cont:
                tokens.insert(i + 1, tokens[i][-len(cont):])
                tokens[i] = tokens[i][:-len(cont)]

    return tokens
