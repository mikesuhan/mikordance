from itertools import chain
from text_processing.concordance import concordance

class CorpusManager():
    corpora = []
    text_count = 0

    def __init__(self, parent):
        self.parent = parent

    def addCorpus(self, corpus):
        self.corpora.append(corpus)
        self.countTexts()

    def countTexts(self):
        self.text_count = sum(len(c.texts) for c in self.corpora)

    def concordance(self, query, corpus_inds, **kwargs):
            texts = [self.corpora[i].texts for i in corpus_inds]
            texts = chain(*texts)
            yield concordance(query, *texts, **kwargs)

    def message(self, msg, status='loading'):
        self.parent.statusMessage(msg, status)