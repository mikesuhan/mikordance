from PyQt5.QtWidgets import QWidget, QLineEdit, QHBoxLayout, QLabel, QCheckBox, QSpacerItem, QSizePolicy
from PyQt5.QtCore import Qt

class ConcordanceOptions(QWidget):
    def __init__(self):
        super().__init__()
        self.initUI()

    def initUI(self):
        self.options_layout = QHBoxLayout()
        self.options_layout.setContentsMargins(0, 0, 0, 0)

        # left concordance length
        small_le_len = 25
        self.options_layout.addWidget(QLabel('Left', self))
        self.conc_left_le = QLineEdit(self)
        self.conc_left_le.setText('10')
        self.conc_left_le.setFixedWidth(small_le_len)
        self.options_layout.addWidget(self.conc_left_le)
        self.options_layout.addItem(QSpacerItem(10, 0, QSizePolicy.Expanding))

        # right concordance length
        self.options_layout.addWidget(QLabel('Right', self))
        self.conc_right_le = QLineEdit(self)
        self.conc_right_le.setText('10')
        self.conc_right_le.setFixedWidth(small_le_len)
        self.options_layout.addWidget(self.conc_right_le)
        self.options_layout.addItem(QSpacerItem(10, 0, QSizePolicy.Expanding))

        # limit
        self.options_layout.addWidget(QLabel('Limit', self))
        self.limit_le = QLineEdit(self)
        self.limit_le.setFixedWidth(small_le_len * 2)
        self.options_layout.addWidget(self.limit_le)
        self.options_layout.addItem(QSpacerItem(10, 0, QSizePolicy.Expanding))

        # shuffle results
        self.options_layout.addWidget(QLabel('Shuffle', self))
        self.shuffle_cb = QCheckBox(self)
        self.options_layout.addWidget(self.shuffle_cb)
        self.options_layout.addItem(QSpacerItem(10, 0, QSizePolicy.Expanding))

        # case sensitive

        self.options_layout.addWidget(QLabel('Match Case', self))
        self.case_cb = QCheckBox(self)
        self.options_layout.addWidget(self.case_cb)
        self.options_layout.setAlignment(Qt.AlignTop)
        self.setLayout(self.options_layout)

    def getOptionValues(self):
        return {
            'left_len':         int(self.conc_left_le.text() if self.conc_left_le.text().isdigit() else 0),
            'right_len':        int(self.conc_right_le.text() if self.conc_right_le.text().isdigit() else 0),
            'limit':            int(self.limit_le.text()) if self.limit_le.text().isdigit() else None,
            'random_order':     self.shuffle_cb.isChecked(),
            'case_sensitive':   self.case_cb.isChecked()
        }