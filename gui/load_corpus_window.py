import os

from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import (QWidget, QMainWindow, QRadioButton, QLabel, QLineEdit, QListWidget,
                             QGridLayout, QPushButton, QHBoxLayout, QFileDialog, QDialog, QMessageBox, QInputDialog)

from gui.get_existing_directories import GetExistingDirectories
from text_processing.corpus import Corpus


class LoadCorpusWindow(QMainWindow):
    """New window showing tabulated concurdance results."""
    def __init__(self, parent):
        super(LoadCorpusWindow, self).__init__(parent)
        self.parent = parent
        self.corpus = None
        self.corpus_loaded = False
        self.initUI()

    def initUI(self):

        # window configuration

        self.setGeometry(920, 100, 260, 400)
        self.setWindowTitle('Load New Corpus')

        grid_layout = QGridLayout()
        grid_layout.setSpacing(10)
        small_le_size = 100

        # corpus name

        name_label = QLabel("Corpus Name (required)", self)
        name_label.setAlignment(Qt.AlignCenter)
        grid_layout.addWidget(name_label, 0, 0, 1, 2, Qt.AlignTop)
        self.name_le = QLineEdit(self)
        grid_layout.addWidget(self.name_le, 1, 0, 1, 2, Qt.AlignTop)

        # encoding

        encoding_label = QLabel("Character Encoding", self)
        grid_layout.addWidget(encoding_label, 2, 0, Qt.AlignTop)
        self.encoding_le = QLineEdit(self)
        self.encoding_le.setFixedWidth(small_le_size)
        grid_layout.addWidget(self.encoding_le, 2, 1, Qt.AlignTop)

        # tag delimiter

        tag_delimiter_label = QLabel("Tag Delimiter", self)
        grid_layout.addWidget(tag_delimiter_label, 3, 0, Qt.AlignTop)
        self.tag_delimiter_le = QLineEdit(self)
        self.tag_delimiter_le.setFixedWidth(small_le_size)
        grid_layout.addWidget(self.tag_delimiter_le, 3, 1, Qt.AlignTop)

        # Words and word-tag pairs separated by:

        tag_delimiter_label = QLabel("Words and word-tag pairs separated by:", self)
        grid_layout.addWidget(tag_delimiter_label, 4, 0, 1, 2, Qt.AlignTop)
        self.spaces_rb = QRadioButton('Spaces')
        self.spaces_rb.setChecked(True)
        grid_layout.addWidget(self.spaces_rb, 5, 0, Qt.AlignTop)
        self.lineb_rb = QRadioButton('Line Breaks')
        grid_layout.addWidget(self.lineb_rb, 5, 1, Qt.AlignTop)

        # add files/folders buttons

        add_buttons_layout = QHBoxLayout()
        add_files_button = QPushButton('Add Files', self)
        add_files_button.clicked.connect(self.addFiles)
        add_buttons_layout.addWidget(add_files_button)
        add_files_button.setFixedWidth(self.width() / 2)
        add_folders_button = QPushButton('Add Folders', self)
        add_folders_button.clicked.connect(self.addFolders)
        add_buttons_layout.addWidget(add_folders_button)
        add_folders_button.setFixedWidth(self.width() / 2)
        buttons_widget = QWidget()
        buttons_widget.setLayout(add_buttons_layout)
        grid_layout.addWidget(buttons_widget, 6, 0, 1, 2, Qt.AlignTop)

        # list box that filenames get added to

        self.files_lw = QListWidget(self)
        self.files_lw.setFixedHeight(300)
        grid_layout.addWidget(self.files_lw, 7, 0, 1, 2, Qt.AlignTop)

        # load button

        self.clear_button = QPushButton('Clear File List', self)
        self.clear_button.clicked.connect(self.files_lw.clear)
        grid_layout.addWidget(self.clear_button, 8, 0, 1, 2, Qt.AlignTop)
        self.load_button = QPushButton('Load Corpus', self)
        self.load_button.clicked.connect(self.load)
        grid_layout.addWidget(self.load_button, 9, 0, 1, 2, Qt.AlignTop)

        # central widget

        central_widget = QWidget()
        central_widget.setLayout(grid_layout)
        self.setCentralWidget(central_widget)

        self.show()

    def showInEditMode(self, corpus_ind):
        self.show()
        self.corpus = self.parent.corpus_manager.corpora[corpus_ind]

        # Set QListEdit widget text values
        les_to_set = (
            (self.name_le, self.corpus.name),
            (self.encoding_le, self.corpus.encoding),
            (self.tag_delimiter_le, self.corpus.tag_delimiter),
        )

        for line_edit, string_val in les_to_set:
            line_edit.setText(string_val)

        # Check for non-default radio button value
        if self.corpus.item_delimiter == '\n':
            self.lineb_rb.setChecked(True)

        # Sets file list
        self.files_lw.addItems(self.corpus.file_paths)

    def load(self):
        # information input into QLineEdit widgets, which is passed to Corpus and Text
        if not self.name_le.text():
            self.showAlert('Missing Corpus Name',
                           'Nothing has been entered in the "Corpus Name" field.',
                           'This is a required field.')

        else:
            self.encoding_le.setText(self.encoding_le.text().strip())
            file_list = []
            for i in range(self.files_lw.count()):
                file_list.append(self.files_lw.item(i).text())

            if not file_list:
                self.showAlert('Missing Files',
                               'No files have been added.',
                               'Use the "Add Files" or "Add Folders" button to select files for your corpus.')
            else:
                if self.corpus is None:
                    self.corpus_updated = False
                    self.corpus = Corpus(self.parent.corpus_manager,
                                         file_list,
                                         self.name_le.text().strip(),
                                         encoding=self.encoding_le.text() if self.encoding_le.text() else None,
                                         tag_delimiter=self.tag_delimiter_le.text(),
                                         item_delimiter=' ' if self.spaces_rb.isChecked() else '\n'
                                         )

                else:
                    self.corpus_updated = True
                    self.corpus.loadTexts(file_list)
                    self.corpus.name = self.name_le.text().strip()
                    self.corpus.encoding = self.encoding_le.text() if self.encoding_le.text() else None
                    self.corpus.tag_delimiter = self.tag_delimiter_le.text()
                    self.corpus.item_delimiter=' ' if self.spaces_rb.isChecked() else '\n'


                self.corpus_loaded = True

                self.name_le.clear()
                self.files_lw.clear()
                self.close()

    def showAlert(self, title, text, info_text, icon=QMessageBox.Critical):
        alert = QMessageBox(self)
        alert.setIcon(icon)
        alert.setWindowTitle(title)
        alert.setText(text)
        alert.setInformativeText(info_text)
        alert.setStandardButtons(QMessageBox.Ok)
        alert.show()

    def addFiles(self):
        """Adds selected files."""
        filepaths = QFileDialog.getOpenFileNames(self, 'Open file', os.curdir)
        if filepaths[0]:
            self.files_lw.addItems(filepaths[0])

    def addFolders(self):
        """Adds every file in selected directories and their subdirectories."""
        dirs = GetExistingDirectories()
        if dirs.exec_() == QDialog.Accepted:
            filepaths = []
            for dir in dirs.selectedFiles():
                for filepath, subdirs, files in os.walk(dir):
                    for f in files:
                        filepaths.append(os.path.join(filepath, f))

            if filepaths:
                self.files_lw.addItems(filepaths)
