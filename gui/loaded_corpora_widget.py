from functools import partial
from PyQt5.QtWidgets import QWidget, QListWidget, QAbstractItemView, QLabel, QVBoxLayout, QPushButton, QHBoxLayout

class LoadedCorporaWidget(QWidget):
    def __init__(self, parent):
        super().__init__(parent)
        self.parent = parent
        self.initUI()

    def initUI(self):
        # corpora layout
        vbox = QVBoxLayout()
        vbox.setContentsMargins(0, 0, 0, 0)

        # corpora listwidget and label
        corpora_label = QLabel('Loaded Corpora', self)
        vbox.addWidget(corpora_label)
        self.corpora_lw = QListWidget()
        self.corpora_lw.setSelectionMode(QAbstractItemView.ExtendedSelection)
        vbox.addWidget(self.corpora_lw)

        # corpora buttons
        load_corpus_button = QPushButton('Load New', self)
        load_corpus_button.clicked.connect(self.parent.openLoadCorpusWindow)
        hbox = QHBoxLayout()
        hbox.addWidget(load_corpus_button)
        edit_corpus_button = QPushButton('Edit Selected', self)
        edit_corpus_button.clicked.connect(partial(self.parent.openLoadCorpusWindow, True))
        hbox.addWidget(edit_corpus_button)
        edit_corpus_button = QPushButton('Remove', self)
        edit_corpus_button.clicked.connect(self.parent.removeCorpus)
        hbox.addWidget(edit_corpus_button)
        buttons = QWidget()
        buttons.setLayout(hbox)
        vbox.addWidget(buttons)

        self.setLayout(vbox)
