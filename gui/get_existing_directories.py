from PyQt5.QtWidgets import QFileDialog, QListView, QTreeView, QAbstractItemView


class GetExistingDirectories(QFileDialog):
    """
    Selects multiple directories in dialog.
    
    From https://stackoverflow.com/questions/18707261/how-to-select-multiple-directories-with-kfiledialog
    """
    def __init__(self, *args):
        super(GetExistingDirectories, self).__init__(*args)
        self.setOption(self.DontUseNativeDialog, True)
        self.setFileMode(self.Directory)
        self.setOption(self.ShowDirsOnly, True)
        self.findChildren(QListView)[0].setSelectionMode(QAbstractItemView.ExtendedSelection)
        self.findChildren(QTreeView)[0].setSelectionMode(QAbstractItemView.ExtendedSelection)