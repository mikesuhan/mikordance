from PyQt5.QtCore import Qt, QCoreApplication
from PyQt5.QtWidgets import (QWidget, QMainWindow, QTableWidget, QTableWidgetItem, QAbstractScrollArea, QVBoxLayout,
                             QHeaderView, QProgressBar)

class TableWidgetItem(QTableWidgetItem):
    def __init__(self, text, sortKey):
            QTableWidgetItem.__init__(self, text, QTableWidgetItem.UserType)
            self.sortKey = sortKey

    #Qt uses a simple < check for sorting items, override this to use the sortKey
    def __lt__(self, other):
            return self.sortKey < other.sortKey

class StatisticsWindow(QMainWindow):
    """New window showing tabulated concurdance results."""
    def __init__(self, parent, results, header, query, **kwargs):
        super(StatisticsWindow, self).__init__(parent)
        self.parent = parent
        self.results = results
        self.header = header
        self.query = query
        self.kwargs = kwargs
        self.initUI()

    def initUI(self):
        # adds table
        vbox = QVBoxLayout()
        self.table_widget = QTableWidget()
        vbox.addWidget(self.table_widget)
        central_widget = QWidget()
        central_widget.setLayout(vbox)
        self.setCentralWidget(central_widget)
        self.populateTable()
        # adjusts cell size in table to fit contents
        self.table_widget.setSizeAdjustPolicy(QAbstractScrollArea.AdjustToContents)
        self.table_widget.resizeColumnsToContents()
        self.table_widget.setHorizontalHeaderLabels(self.header)
        self.table_widget.horizontalHeader().setSectionResizeMode(QHeaderView.Stretch)
        self.table_widget.setSortingEnabled(True)
        # configures window
        self.setWindowTitle('Statistics')


    def populateTable(self):
        row = 0
        # makes sure table is clear before adding anything to it
        self.table_widget.setRowCount(0)

        for line in sorted(self.results, key=lambda x: x[1], reverse=True):
            # print(line[0])
            self.table_widget.setRowCount(row + 1)
            self.table_widget.setColumnCount(len(line))

            for col, cell in enumerate(line):
                cell = TableWidgetItem(str(cell), cell)
                cell.setTextAlignment(int(Qt.AlignLeft | Qt.AlignVCenter))
                self.table_widget.setItem(row, col, cell)
            row += 1




        if row > 0:
            self.showMaximized()
        else:
            query = 'or'.join(' '.join(q) for q in self.query)
            self.parent.statusMessage('No results found for "{}".'.format(query), 'alert')