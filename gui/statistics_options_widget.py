from itertools import chain

from PyQt5.QtWidgets import (QWidget, QLineEdit, QHBoxLayout, QLabel, QCheckBox,
                             QSpacerItem, QSizePolicy, QVBoxLayout, QGridLayout)
from PyQt5.QtCore import Qt


class StatisticsOptions(QWidget):
    def __init__(self):
        super().__init__()
        self.initUI()

    def initUI(self):
        self.options_layout = QHBoxLayout()
        self.options_layout.setContentsMargins(0, 0, 0, 0)

        # left concordance length
        small_le_len = 25
        self.options_layout.addWidget(QLabel('Min. Length', self))
        self.min_ngram_le = QLineEdit(self)
        self.min_ngram_le.setText('1')
        self.min_ngram_le.setFixedWidth(small_le_len)
        self.options_layout.addWidget(self.min_ngram_le)
        self.options_layout.addItem(QSpacerItem(10, 0, QSizePolicy.Expanding))

        # right concordance length
        self.options_layout.addWidget(QLabel('Max Length', self))
        self.max_ngram_le = QLineEdit(self)
        self.max_ngram_le.setText('1')
        self.max_ngram_le.setFixedWidth(small_le_len)
        self.options_layout.addWidget(self.max_ngram_le)
        self.options_layout.addItem(QSpacerItem(10, 0, QSizePolicy.Expanding))

        # limit
        self.options_layout.addWidget(QLabel('Limit', self))
        self.limit_le = QLineEdit(self)
        self.limit_le.setFixedWidth(small_le_len * 2)
        self.options_layout.addWidget(self.limit_le)
        self.options_layout.addItem(QSpacerItem(10, 0, QSizePolicy.Expanding))

        # case sensitive

        self.options_layout.addWidget(QLabel('Match Case', self))
        self.case_cb = QCheckBox(self)
        self.options_layout.addWidget(self.case_cb)
        self.options_layout.setAlignment(Qt.AlignTop)
        self.options_widget = QWidget()
        self.options_widget.setLayout(self.options_layout)

        self.options_vbox_layout = QVBoxLayout()
        self.options_vbox_layout.setContentsMargins(0, 0, 0, 0)
        self.options_vbox_layout.addWidget(self.options_widget)

        self.stats_names = [
            [('Rate', True), ('Frequency', True), ('Dispersion', True)],
            [('Minimum', False), ('Maximum', False), ('Range', False)],
            [('Mean', False), ('Median', False), ('Mode', False)],
            [('Sample SD', False), ('Pop. SD', False)],
            [('Sample Variance', False), ('Pop. Variance', False)]]

        self.stats_cbs = {}

        stats_layout = QGridLayout()
        stats_layout.setContentsMargins(20, 10, 20, 10)

        for r, row in enumerate(self.stats_names):
            offset = 0
            for c, (name, checked) in enumerate(row):
                stats_layout.addWidget(QLabel(name), r, c + offset)
                self.stats_cbs[name] = QCheckBox()
                self.stats_cbs[name].setChecked(checked)
                stats_layout.addWidget(self.stats_cbs[name], r, c + 1 + offset)
                offset += 1

        self.options_vbox_layout.setAlignment(Qt.AlignTop)
        self.options_vbox_layout.addItem(QSpacerItem(25, 25))
        stats_widget = QWidget()
        stats_widget.setLayout(stats_layout)

        self.options_vbox_layout.addWidget(stats_widget)

        self.setLayout(self.options_vbox_layout)

    def getOptionValues(self):
        values = {
            'min_ngram_len':         int(self.min_ngram_le.text() if self.min_ngram_le.text().isdigit() else 0),
            'max_ngram_len':        int(self.max_ngram_le.text() if self.max_ngram_le.text().isdigit() else 0),
            'limit':            int(self.limit_le.text()) if self.limit_le.text().isdigit() else None,
            'case_sensitive':   self.case_cb.isChecked()
        }

        values['procedures'] = []
        for stat_name, _ in chain(*self.stats_names):
            if self.stats_cbs[stat_name].isChecked():
                values['procedures'].append(stat_name)

        return values

