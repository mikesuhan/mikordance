from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import (QWidget, QMainWindow, QLineEdit, QTableWidgetItem, QAbstractScrollArea,
                             QVBoxLayout, QTextEdit)
from PyQt5 import QtGui
from text_processing.substring_from_tokens import get_substring_inds, emphasize_substring

class TextWindow(QMainWindow):
    """New window showing tabulated concurdance results."""
    def __init__(self, parent, text, token_i, token_right_i):
        print(text.filepath)
        super(TextWindow, self).__init__(parent,)
        self.parent = parent
        self.text = text
        self.substring_inds = get_substring_inds(text.text, text.tokens(), token_i, token_right_i)
        self.initUI()

    def initUI(self):
        self.setGeometry(200, 200, 800, 600)

        vbox = QVBoxLayout()

        # Top field with filename in it
        fname_le = QLineEdit(self)
        fname_le.setText(self.text.filepath)
        vbox.addWidget(fname_le)

        # Text area
        text_le = QTextEdit(self)
        text_le.setReadOnly(True)
        text_le.insertHtml(emphasize_substring(self.text.text, self.substring_inds))
        vbox.addWidget(text_le)

        # Central widget
        central_widget = QWidget()
        central_widget.setLayout(vbox)
        self.setCentralWidget(central_widget)
