from PyQt5.QtCore import Qt
from PyQt5.QtWidgets import (QWidget, QMainWindow, QTableWidget, QTableWidgetItem, QAbstractScrollArea,
                             QVBoxLayout, QHeaderView)
from PyQt5 import QtGui

class ConcordanceWindow(QMainWindow):
    """New window showing tabulated concurdance results."""
    def __init__(self, parent, query, corpus_inds, **kwargs):
        super(ConcordanceWindow, self).__init__(parent)
        self.parent = parent
        self.query = query
        self.corpus_inds = corpus_inds
        self.kwargs = kwargs
        self.initUI()

    def initUI(self):
        self.setGeometry(200, 200, 800, 600)

        # adds table
        vbox = QVBoxLayout()
        self.table_widget = QTableWidget()
        vbox.addWidget(self.table_widget)
        central_widget = QWidget()
        central_widget.setLayout(vbox)
        self.setCentralWidget(central_widget)
        self.populateTable()
        # adjusts cell size in table to fit contents
        self.table_widget.setSizeAdjustPolicy(QAbstractScrollArea.AdjustToContents)
        self.table_widget.resizeColumnsToContents()
        self.table_widget.setHorizontalHeaderLabels(['Corpus', 'Left', '', 'Right'])
        self.table_widget.itemDoubleClicked.connect(self.parent.showText)
        # configures window
        self.setWindowTitle('Concordance')


    def populateTable(self):
        alignments = (int(Qt.AlignLeft | Qt.AlignVCenter),
                      int(Qt.AlignRight | Qt.AlignVCenter),
                      int(Qt.AlignHCenter | Qt.AlignVCenter),
                      int(Qt.AlignLeft | Qt.AlignVCenter))


        concordance_line_generators = self.parent.corpus_manager.concordance(self.query,
                                                                      self.corpus_inds,
                                                                      **self.kwargs)
        row = 0

        for concordance_lines in concordance_line_generators:

            for line in concordance_lines:
                self.table_widget.setRowCount(row + 1)
                self.table_widget.setColumnCount(4)

                # line is tuple  (Text, str, str, str, int, int)
                for col, cell in enumerate(line[:4]):
                    if col == 0:
                        cell = cell.name

                    cell = QTableWidgetItem(cell)
                    cell.setTextAlignment(alignments[col])

                    if col == 2:
                        cell.text = line[0]
                        cell.token_i = line[4]
                        cell.token_end_i = line[5]
                    self.table_widget.setItem(row, col, cell)

                row += 1

        if row > 0:
            self.showMaximized()
        else:
            self.parent.statusMessage('No results found for "{}".'.format(self.query), 'alert')

