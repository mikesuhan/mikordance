from PyQt5.QtWidgets import QWidget, QComboBox, QPushButton, QLineEdit, QHBoxLayout

class SearchOptionsWidget(QWidget):

    def __init__(self, parent):
        super().__init__(parent)
        self.parent = parent
        self.initUI()

    def initUI(self):
        top_row_hbox = QHBoxLayout()

        self.results_combo = QComboBox(self)
        self.results_combo.addItem('Concordance')
        self.results_combo.addItem('Statistics')
        self.results_combo.addItem('Ngrams')
        self.results_combo.currentTextChanged.connect(self.parent.onResultsComboChange)

        top_row_hbox.addWidget(self.results_combo)

        search_button = QPushButton("Search")
        search_button.clicked.connect(self.parent.onSearchPress)
        self.search_line_edit = QLineEdit()



        top_row_hbox.addWidget(self.search_line_edit)
        top_row_hbox.addWidget(search_button)
        self.setLayout(top_row_hbox)

    def searchText(self):
        return self.search_line_edit.text()

    def comboText(self):
        return self.results_combo.currentText()