import sys

from PyQt5.QtWidgets import (QWidget, QMainWindow, QVBoxLayout, QApplication, qApp, QProgressBar,
                             QAction, QStackedWidget)
from PyQt5.QtCore import Qt
from gui.concordance_window import ConcordanceWindow
from gui.statistics_window import StatisticsWindow
from gui.load_corpus_window import LoadCorpusWindow
from gui.text_window import TextWindow
from text_processing.corpus_manager import CorpusManager
from text_processing.tokenizers import word_tokenize
from gui.concordance_options_widget import ConcordanceOptions
from gui.loaded_corpora_widget import LoadedCorporaWidget
from gui.search_options_widget import SearchOptionsWidget
from gui.statistics_options_widget import StatisticsOptions

class MainMenu(QMainWindow):
    def __init__(self):
        super().__init__()
        self.results_format = 'Concordance'
        self.corpus_manager = CorpusManager(self)
        self.corpora = []
        self.initUI()

    def initUI(self):

        self.setWindowTitle('Mikordance')
        self.setGeometry(100, 100, 480, 600)

        self.status = self.statusBar()
        self.statusMessage('No files have been loaded.')

        # menu bar

        menu_bar = self.menuBar()
        file_menu = menu_bar.addMenu('&File')

        load_act = QAction('Load corpus', self)
        load_act.setShortcut('Ctrl+L')
        load_act.setStatusTip('Load corpus')
        file_menu.addAction(load_act)
        load_act.triggered.connect(self.openLoadCorpusWindow)

        exit_act = QAction('Exit', self)
        exit_act.setShortcut('Ctrl+Q')
        exit_act.setStatusTip('Exit application')
        exit_act.triggered.connect(qApp.quit)
        file_menu.addAction(exit_act)

        ############### Menu

        vbox = QVBoxLayout()

        # search area

        self.search_options = SearchOptionsWidget(self)
        vbox.addWidget(self.search_options)

        # options widgets

        self.conc_options_widget = ConcordanceOptions()
        self.statistics_options_widget = StatisticsOptions()
        self.options_stacked_widget = QStackedWidget()
        self.options_stacked_widget.addWidget(self.conc_options_widget)
        self.options_stacked_widget.addWidget(self.statistics_options_widget)
        # Placeholders for other option thingies
        self.options_stacked_widget.addWidget(QWidget())
        self.options_stacked_widget.addWidget(QWidget())
        vbox.addWidget(self.options_stacked_widget)

        # corpora loading listbox and buttons

        self.loaded_corpora_widget = LoadedCorporaWidget(self)
        vbox.addWidget(self.loaded_corpora_widget)

        vbox.setAlignment(Qt.AlignTop)

        # adds to main window

        central_widget = QWidget()
        central_widget.setLayout(vbox)
        self.setCentralWidget(central_widget)

        self.load_corpus_window = LoadCorpusWindow(self)
        self.load_corpus_window.show()
        self.load_corpus_window.load_button.clicked.connect(self.loadCorpus)
        self.show()

    def onResultsComboChange(self):
        options_widgets = {
            'Concordance': 0,
            'Statistics': 1,
            'Ngrams': 2,
        }

        self.options_stacked_widget.setCurrentIndex(options_widgets[self.search_options.comboText()])

    def onSearchPress(self):
        """Determines what happens when search is pressed based on selection in self.search_options.results_combo combo box."""
        # requires corpus to be loaded to do anything
        if self.corpus_manager.text_count:
            # makes list of selected corpora indices, making value [0] if only one is loaded
            if self.loaded_corpora_widget.corpora_lw.count() == 1:
                selection_inds = [0]
            elif self.loaded_corpora_widget.corpora_lw.count() > 1:
                selection_inds = [i.row() for i in self.loaded_corpora_widget.corpora_lw.selectedIndexes()]
            else:
                selection_inds = []

            # requires corpus to be selected if there is more than one loaded
            if selection_inds:
                if self.search_options.comboText() == 'Concordance':
                    # requires search term for concordance search
                    if self.search_options.searchText():
                        conc_options = self.conc_options_widget.getOptionValues()
                        conc_options['tokenizer'] = word_tokenize
                        ConcordanceWindow(self,
                                           self.search_options.searchText().strip(),
                                           selection_inds,
                                           **conc_options)

                    else:
                        self.statusMessage('No search term has been entered.', 'alert')

                elif self.search_options.comboText() == 'Statistics':
                    query = tuple(word_tokenize(self.search_options.searchText().strip()))
                    # makes string into tuple of tuples if it's not whitespace
                    if query:
                        query = query,

                    stats_options = self.statistics_options_widget.getOptionValues()
                    ngram_lens = (stats_options['min_ngram_len'], stats_options['max_ngram_len'])
                    results = self.corpus_manager.corpora[selection_inds[0]].stats(
                                                                    ngram_lens,
                                                                    *query,
                                                                    procedures_list=stats_options['procedures'])
                    header = [''] + stats_options['procedures']
                    StatisticsWindow(self, results, header, query)

            else:
                self.statusMessage('At least one corpus must be selected.', 'alert')

        else:
            self.statusMessage(
                'You must load files before using the {} function'.format(self.results_format.lower()), 'alert')


    def statusMessage(self, message, style='normal'):
        styles = {
            'alert': 'QStatusBar{color:red;}',
            'normal': 'QStatusBar{color:black;}',
            'loading': 'QStatusBar{color:black;}'
        }
        self.status.setStyleSheet(styles.get(style, 'normal'))
        self.status.showMessage(message)

    def openLoadCorpusWindow(self, edit=False):
        if edit:
            selection_ind = None
            if self.loaded_corpora_widget.corpora_lw.count() == 1:
                selection_ind = 0
            elif self.loaded_corpora_widget.corpora_lw.count() == 0:
                self.statusMessage('No corpora are loaded.', 'alert')
            else:
                selection_inds = [i.row() for i in self.loaded_corpora_widget.corpora_lw.selectedIndexes()]
                if len(selection_inds) == 1:
                    selection_ind = selection_inds[0]
                elif len(selection_inds) > 1:
                    self.statusMessage('Too many corpora selected.')
                elif not selection_inds:
                    self.statusMessage('No corpus is selected.')
            if selection_ind is not None:
                self.load_corpus_window.showInEditMode(selection_ind)
        else:
            self.load_corpus_window.show()
        

    def loadCorpus(self):
        """Makes user input from CorpusWindow into a Corpus object."""
        if self.load_corpus_window.corpus_loaded:
            self.load_corpus_window.corpus_loaded = False

            # changes or adds name to listwidget
            if self.load_corpus_window.corpus_updated:
                lw_item_ind = self.corpus_manager.corpora.index(self.load_corpus_window.corpus)
                lw_item = self.loaded_corpora_widget.corpora_lw.item(lw_item_ind)
                lw_item.setText(self.load_corpus_window.corpus.name)
            else:
                self.loaded_corpora_widget.corpora_lw.addItem(self.load_corpus_window.corpus.name)
                self.corpus_manager.addCorpus(self.load_corpus_window.corpus)

            self.load_corpus_window.corpus = None
            self.statusMessage('{0} files loaded.'.format(self.corpus_manager.text_count))

    def removeCorpus(self):
        if self.corpus_manager.text_count:
            # makes list of selected corpora indices, making value [0] if only one is loaded
            if self.loaded_corpora_widget.corpora_lw.count() == 1:
                selection_inds = [0]
            elif self.loaded_corpora_widget.corpora_lw.count() > 1:
                selection_inds = [i.row() for i in self.loaded_corpora_widget.corpora_lw.selectedIndexes()]
            else:
                selection_inds = None

            if selection_inds:
                n = 0
                for ind in selection_inds:
                    ind -= n
                    del self.corpus_manager.corpora[ind]
                    item = self.loaded_corpora_widget.corpora_lw.takeItem(ind)
                    item = None
                    n += 1
                self.corpus_manager.countTexts()


            else:
                self.statusMessage('At least one corpus must be selected.', 'alert')
        else:
            self.statusMessage('There is nothing to remove.', 'alert')

    def showText(self, q_table_item):
        text_window = TextWindow(self, q_table_item.text, q_table_item.token_i, q_table_item.token_end_i)
        text_window.show()

if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = MainMenu()
    sys.exit(app.exec_())